FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/PipelineDemo-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
